<?php include ROOT . '/views/layouts/header.php'; ?>

<?php if ($result): ?>
    <div class="small_text center">Ви успішно зареєстровані!</div>
<?php else: ?>
    <?php if (isset($errors) && is_array($errors)): ?>
        <div class="messages center">
            <ul>
                <?php foreach ($errors as $error): ?>
                    <li> - <?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <form action="#" method="post" class="reg_form center">
        <input type="text" name="name" placeholder="Ім'я" value="<?php echo $name;?>"/>
        <input type="email" name="email" placeholder="E-mail" value="<?php echo $email;?>"/>
        <input type="password" name="password" placeholder="Пароль" value="<?php echo $password;?>"/>
        <input type="submit" name="submit" id="reg_btn" value="Реєстрація" />
    </form>
    <div class="answer center">Уже зареєстровані?</div>
    <a class="login center" href="/user/login">Вхід</a>

<?php endif; ?>

<?php include ROOT . '/views/layouts/footer.php'; ?>