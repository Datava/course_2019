<?php include ROOT . '/views/layouts/header.php'; ?>

<?php if (isset($errors) && is_array($errors)): ?>
    <div class="messages center">
        <ul>
            <?php foreach ($errors as $error): ?>
                <li> - <?php echo $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<form action="#" method="post" class="reg_form center">
    <input type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
    <input type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
    <input type="submit" name="submit" id="reg_btn" value="Вхід" />
</form>
<div class="answer center">Не зареєстровані?</div>
<a class="login center" href="/user/register">Реєстрація</a>

<?php include ROOT . '/views/layouts/footer.php'; ?>