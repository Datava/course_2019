<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="big_text center">Замовлення:</div>

<table class="center">
    <tr>
        <th>id</th>
        <th>І'мя покупця</th>
        <th>Телефон</th>
        <th>Дата оформлення</th>
        <th>Статус</th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($ordersList as $order): ?>
        <tr>
            <td><?php echo $order['id']; ?></td>
            <td><?php echo $order['user_name']; ?></td>
            <td><?php echo $order['user_phone']; ?></td>
            <td><?php echo $order['date']; ?></td>
            <td><?php echo Order::getStatusText($order['status']); ?></td>
            <td><a class="no_link" href="/admin/order/view/<?php echo $order['id']; ?>" title="змінити">дивитись</a></td>
            <td><a class="no_link" href="/admin/order/update/<?php echo $order['id']; ?>" title="змінити">змінити</a></td>
            <td><a class="no_link" href="/admin/order/delete/<?php echo $order['id']; ?>" title="видалити">видалити</a></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php include ROOT . '/views/layouts/footer.php'; ?>

