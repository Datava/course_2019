<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="big_text center">Перегляд замовлення №<?php echo $order['id']; ?></div>

<div class="small_text center">Інформація про замовлення:</div>
            <table class="center" id="order_table">
                <tr>
                    <td>Номер замовлення:</td>
                    <td><?php echo $order['id']; ?></td>
                </tr>
                <tr>
                    <td>Ім'я клієнта:</td>
                    <td><?php echo $order['user_name']; ?></td>
                </tr>
                <tr>
                    <td>Телефон:</td>
                    <td><?php echo $order['user_phone']; ?></td>
                </tr>
                <tr>
                    <td>Комментар:</td>
                    <td><?php echo $order['user_comment']; ?></td>
                </tr>
                <?php if ($order['user_id'] != 0): ?>
                    <tr>
                        <td>id кліента:</td>
                        <td><?php echo $order['user_id']; ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td><b>Статус замовлення:</b></td>
                    <td><?php echo Order::getStatusText($order['status']); ?></td>
                </tr>
                <tr>
                    <td><b>Дата замовлення:</b></td>
                    <td><?php echo $order['date']; ?></td>
                </tr>
            </table>

<br>
<div class="small_text center">Замовлені товари:</div>

            <table class="center">
                <tr>
                    <th>id товару</th>
                    <th>Назва</th>
                    <th>Ціна</th>
                    <th>Кількість</th>
                </tr>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?php echo $product['id']; ?></td>
                        <td><a class="no_link" href="/product/<?php echo $product['id'] ;?>"><?php echo $product['name']; ?></a></td>
                        <td><?php echo $product['cost']; ?></td>
                        <td><?php echo $productsQuantity[$product['id']]; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>


<?php include ROOT . '/views/layouts/footer.php'; ?>

