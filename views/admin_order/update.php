<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="big_text center">Редагувати замовлення #<?php echo $id; ?></div>

        <form class="reg_form center" action="#" method="post">

            <div class="center">Ім'я клієнта</div>
            <input type="text" name="user_name" placeholder="" value="<?php echo $order['user_name']; ?>">

            <div class="center">Телефон</div>
            <input type="text" name="user_phone" placeholder="" value="<?php echo $order['user_phone']; ?>">

            <div class="center">Коментар</div>
            <input type="text" name="user_comment" placeholder="" value="<?php echo $order['user_comment']; ?>">

            <p>Статус</p>
            <select name="status">
                <option value="1" <?php if ($order['status'] == 1) echo ' selected="selected"'; ?>>Нове замовлення</option>
                <option value="2" <?php if ($order['status'] == 2) echo ' selected="selected"'; ?>>Опрацьовується</option>
                <option value="3" <?php if ($order['status'] == 3) echo ' selected="selected"'; ?>>В дорозі</option>
                <option value="4" <?php if ($order['status'] == 4) echo ' selected="selected"'; ?>>Доставлено</option>
            </select>
            <br>
            <br>
            <input type="submit" name="submit" id="reg_btn" value="Сохранить">
        </form>

<?php include ROOT . '/views/layouts/footer.php'; ?>

