<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="flex_row">
<div class="column">
<?php foreach ($categories as $categoryItem): ?>
        <a class="link" href="/category/<?php echo $categoryItem['id']; ?>"><?php echo $categoryItem['name']; ?></a>
<?php endforeach; ?>
</div>
    <div class="slider">
        <?php foreach ($newsList as $newsItem):?>
            <a class="slide show" href='/news/<?php echo $newsItem['id'] ;?>'><img alt="" src="/upload/images/news/<?php echo $newsItem['image'];?>"></a>
        <?php endforeach;?>
    </div>
</div>

<p class="big_text">Популярні товари</p>
<div class="flex_row">
<?php foreach ($popularProducts as $product):?>
    <div class="product">
        <a href="product/<?php echo $product['id'] ;?>"><img alt="<?php echo $product['name'];?>" src="/upload/images/products/<?php echo $product['image'];?>"></a>
        <a class="product_name" href="product/<?php echo $product['id'] ;?>"><?php echo $product['name'];?></a>
        <div class="product_info">Виробник: <?php echo $product['brand'];?></div>
        <div class="product_info">Країна: <?php echo $product['country'];?></div>
        <div class="cost center"><?php echo $product['cost'];?>-</div>
        <a href="#" class="to_cart center" data-id="<?php echo $product['id'];?>">до кошику</a>
    </div>
<?php endforeach; ?>
</div>

<script src="/template/js/slider.js"></script>

<?php include ROOT . '/views/layouts/footer.php'; ?>