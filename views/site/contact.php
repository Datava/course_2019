<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="big_text center">Де ми знаходимось?</div>

<div class="center">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2551.5329373397926!2d28.635336316020986!3d50.244629979447446!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472c6493ebf252ed%3A0xc400e72454e33b55!2z0JTQtdGA0LbQsNCy0L3QuNC5INGD0L3RltCy0LXRgNGB0LjRgtC10YIgwqvQltC40YLQvtC80LjRgNGB0YzQutCwINC_0L7Qu9GW0YLQtdGF0L3RltC60LDCuw!5e0!3m2!1suk!2sua!4v1558740912735!5m2!1suk!2sua" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<div class="big_text center">Контактні телефони:</div>
<div class="center">
    <div class="contacts">+380681234567 - Київстар</div>
    <div class="contacts">+380951212121 - МТС</div>
    <div class="contacts">+380932121212 - Vodafone</div>
</div>


<?php include ROOT . '/views/layouts/footer.php'; ?>