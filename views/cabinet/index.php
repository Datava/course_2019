<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="big_text center">Кабінет користувача</div>

<div class="small_text center">Вітаю, <?php echo $user['name'];?>!</div>

<div class="messages center">
    <ul>
        <li><a href="/cabinet/edit/">Редагувати дані</a></li>
        <!--<li><a href="/cabinet/history">Список покупок</a></li>-->

    </ul>
</div>

<br>

<div class="invisibility center">
    <?php if (AdminBase::checkAdmin()): ?>
        <a class="small_text" href="/admin/">Адмін-панель</a>
    <?php endif; ?>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>