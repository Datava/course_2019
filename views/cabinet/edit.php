<?php include ROOT . '/views/layouts/header.php'; ?>

<?php if ($result): ?>
    <div class="small_text center">Дані відредаговано!</div>
    <?php else: ?>
        <?php if (isset($errors) && is_array($errors)): ?>
            <ul class="messages">
                <?php foreach ($errors as $error): ?>
                    <li> - <?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

            <div class="big_text center">Редагування даних</div>
            <form action="#" method="post" class="reg_form center">
                <input type="text" name="name" placeholder="Ім'я" value="<?php echo $name; ?>"/>
                <input type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
                <input type="submit" name="submit" id="reg_btn" value="Зберегти" />
            </form>

<?php endif; ?>

<?php include ROOT . '/views/layouts/footer.php'; ?>