<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="big_text center">Додати категорію</div>

<?php if (isset($errors) && is_array($errors)): ?>
    <ul class="messages center">
        <?php foreach ($errors as $error): ?>
            <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

        <form class="reg_form center" action="#" method="post">

            <div class="center">Назва</div>
            <input type="text" name="name" placeholder="" value="">

            <div class="center">Номер</div>
            <input type="text" name="sort_order" placeholder="" value="">

            <input type="submit" name="submit" id="reg_btn" value="Зберегти">
        </form>

<?php include ROOT . '/views/layouts/footer.php'; ?>

