<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="center"><a href="/admin/category/create" class="small_text">Додати категорію</a></div>

<div class="big_text center">Список категорій:</div>
<br/>

<table class="center">
    <tr>
        <th>id</th>
        <th>Назва</th>
        <th>Номер</th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($categoriesList as $category): ?>
        <tr>
            <td><?php echo $category['id']; ?></td>
            <td><?php echo $category['name']; ?></td>
            <td><?php echo $category['sort_order']; ?></td>
            <td><a class="no_link" href="/admin/category/update/<?php echo $category['id']; ?>" title="змінити">змінити</i></a></td>
            <td><a class="no_link" href="/admin/category/delete/<?php echo $category['id']; ?>" title="видалити">видалити</i></a></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php include ROOT . '/views/layouts/footer.php'; ?>

