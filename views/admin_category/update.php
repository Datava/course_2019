<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="small_text center">Редагувати категорію "<?php echo $category['name']; ?>"</div>

        <form class="reg_form center" action="#" method="post">

            <div class="center">Назва</div>
            <input type="text" name="name" placeholder="" value="<?php echo $category['name']; ?>">

            <div class="center">Порядковий номер</div>
            <input type="text" name="sort_order" placeholder="" value="<?php echo $category['sort_order']; ?>">

            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
        </form>

<?php include ROOT . '/views/layouts/footer.php'; ?>

