</div>
<div class="footer center">
    <div class="copyright">Yuvo 2019</div>
    <div class="llo_footer"></div>
</div>

<script src="/template/js/jquery.js"></script>

<script>
    $(document).ready(function(){
        $(".to_cart").click(function () {
            let id = $(this).attr("data-id");
            $.post("/cart/addAjax/" + id, {}, function (data) {
                $("#cart-count").html(data);
            });
            return false;
        });
    });
</script>

</body>
</html>