<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Music store</title>
    <link href="/template/css/styles.css" rel="stylesheet">
    <link href="/template/css/news.css" rel="stylesheet">
    <link href="/template/css/slider.css" rel="stylesheet">
    <link href="/template/css/registration.css" rel="stylesheet">
    <link href="/template/css/product.css" rel="stylesheet">
</head>
<body>
<div class="pattern"></div>
<div class="background"></div>
<div class="header">
    <div class="in_header center">
        <div class="llo"></div>
        <a class="store" href="/"></a>
        <?php if (User::isGuest()): ?>
            <a class="icon unlock" href="/user/login"></a>
        <?php else: ?>
            <a class="icon lock" href="/user/logout"></a>
            <a class="icon edit" href="/cabinet"></a>
        <?php endif; ?>
        <a class="icon count" id="cart-count" href="/cart/"><?php echo Cart::countItems();?></a>
        <a class="icon cart" href="/cart/"></a>
    </div>
</div>
<div class="big_text" id="test"></div>
<div class="main center">
    <div class="row flex_row">
        <a class="link" href="/">Головна</a>
        <a class="link" href="/catalog/">Каталог</a>
        <a class="link" href="/news/">Новини</a>
        <a class="link" href="/about/">Про нас</a>
        <a class="link" href="/contacts/">Контакти</a>
        <a class="link" href="/search/scan/">Пошук</a>
    </div>