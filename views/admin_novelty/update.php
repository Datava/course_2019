<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="small_text center">Редагувати новину ""</div>

<form class="reg_form center" action="#" method="post">

    <div class="center">Назва</div>
    <input type="text" name="alt" placeholder="" value="<?php echo $novelty['alt']; ?>">

    <div class="center">Текст</div>
    <textarea name="text"><?php echo $novelty['text']; ?></textarea>

    <div class="center">Зображення</div>
    <input type="text" name="image" placeholder="" value="<?php echo $novelty['image']; ?>">

    <input type="submit" name="submit" id="reg_btn" value="Зберегти">

</form>

<?php include ROOT . '/views/layouts/footer.php'; ?>

