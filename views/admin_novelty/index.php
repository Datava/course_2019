<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="center"><a href="/admin/novelty/create" class="small_text">Додати новину</a></div>


<div class="big_text center">Список новин:</div>
<br/>

<table class="center">
    <tr>
        <th>id</th>
        <th>Назва</th>
        <th>Зображення</th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($newsList as $news): ?>
        <tr>
            <td><?php echo $news['id']; ?></td>
            <td><a class="no_link" href="/news/<?php echo $news['id'] ;?>"><?php echo $news['alt']; ?></a></td>
            <td><?php echo $news['image']; ?></td>
            <td><a class="no_link" href="/admin/novelty/update/<?php echo $news['id']; ?>" title="змінити">змінити</i></a></td>
            <td><a class="no_link" href="/admin/novelty/delete/<?php echo $news['id']; ?>" title="видалити">видалити</i></a></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php include ROOT . '/views/layouts/footer.php'; ?>

