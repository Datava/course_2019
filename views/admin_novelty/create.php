<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="big_text center">Додати новину</div>

<?php if (isset($errors) && is_array($errors)): ?>
    <ul class="messages center">
        <?php foreach ($errors as $error): ?>
            <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

        <form class="reg_form center" action="#" method="post">

            <div class="center">Назва</div>
            <input type="text" name="alt" placeholder="" value="">

            <div class="center">Текст</div>
            <textarea name="text"></textarea>

            <div class="center">Зображення</div>
            <input type="text" name="image" placeholder="" value="">

            <input type="submit" name="submit" id="reg_btn" value="Зберегти">
        </form>

<?php include ROOT . '/views/layouts/footer.php'; ?>

