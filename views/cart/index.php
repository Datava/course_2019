<?php include ROOT . '/views/layouts/header.php'; ?>
<div class="big_text">Кошик</div>
                    
<?php if ($productsInCart): ?>
    <div class="small_text">Вибрані товари:</div>
    <table class="table center">
        <tr>
            <th>Назва</th>
            <th>Вартість</th>
            <th>Кількість, шт</th>
            <th>Видалити</th>
        </tr>
        <?php foreach ($products as $product): ?>
            <tr>
                <td>
                    <a href="/product/<?php echo $product['id'];?>">
                        <?php echo $product['name'];?>
                    </a>
                </td>
                <td><?php echo $product['cost'];?>-</td>
                <td><?php echo $productsInCart[$product['id']];?></td>
                <td>
                    <a class="a_link center" href="/cart/delete/<?php echo $product['id'];?>">
                        х
                    </a>

                </td>
            </tr>
        <?php endforeach; ?>
            <tr>
                <td colspan="4">Загальна вартість:</td>
                <td><?php echo $totalPrice;?>-</td>
            </tr>

    </table>

    <a class="a_link center" href="/cart/checkout">Оформити замовлення</a>
<?php else: ?>
    <div class="a_link center">Кошик пустий</div>

    <a class="a_link center" href="/">Повернутися до покупок</a>
<?php endif; ?>


<?php include ROOT . '/views/layouts/footer.php'; ?>