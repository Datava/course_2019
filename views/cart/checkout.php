<?php include ROOT . '/views/layouts/header.php'; ?>

<?php if ($result): ?>
    <div class="small_text center">Замовлення оформлено. Ми вам передзвонимо.</div>
<?php else: ?>

    <div class="a_link center">Вибрано товарів: <?php echo $totalQuantity; ?>, вартістю: <?php echo $totalPrice; ?>-</div>

<?php if (isset($errors) && is_array($errors)): ?>
    <div class="messages center">
        <ul>
            <?php foreach ($errors as $error): ?>
                <li> - <?php echo $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div class="a_link center">Для оформлення замовлення заповніть форму, а ми невдовзі з вами звяжемось.</div>

    <form class="reg_form center" action="#" method="post">

        <input type="text" name="userName" placeholder="Ім'я" value="<?php echo $userName; ?>"/>

        <input type="text" name="userPhone" placeholder="Номер телефону" value="<?php echo $userPhone; ?>"/>

        <input type="text" name="userComment" placeholder="Коментар" value="<?php echo $userComment; ?>"/>

        <input type="submit" name="submit" id="reg_btn" value="Оформити" />
    </form>
</div>

<?php endif; ?>

<?php include ROOT . '/views/layouts/footer.php'; ?>