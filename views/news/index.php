<?php include ROOT . '/views/layouts/header.php'; ?>


<?php foreach ($newsList as $newsItem):?>
    <a class="image_link" href='/news/<?php echo $newsItem['id'] ;?>'>
        <img alt="<?php echo $newsItem['alt'];?>" src="/upload/images/news/<?php echo $newsItem['image'];?>">
        <div class="review">
            <?php echo $newsItem['text'];?>
        </div>
    </a>
<?php endforeach;?>


<?php include ROOT . '/views/layouts/footer.php'; ?>
