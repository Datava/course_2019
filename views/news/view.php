<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="big_text"><?php echo $newsItem['alt']; ?></div>
<img class="news_image center" src="/upload/images/news/<?php echo $newsItem['image'];?>">
<div class="description center"><?php echo $newsItem['text']; ?></div>

<?php include ROOT . '/views/layouts/footer.php'; ?>