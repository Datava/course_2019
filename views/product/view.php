<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="big_text"><?php echo $product['name'];?></div>
<div class="info_wrapper">
    <div class="image_wrapper">
        <img class="product_block" src="/upload/images/products/<?php echo $product['image'];?>">
    </div>
    <div class="product_block">
        <div class="full_info">
            <div>Виробник: <?php echo $product['brand'];?></div>
            <div>Країна: <?php echo $product['country'];?></div>
            <div>Розмір: <?php echo $product['size'];?></div>
            <div>Матеріали: <?php echo $product['materials'];?></div>
            <div class="cost">
                <div class="big_text"><?php echo $product['cost'];?>-</div>
                <a href="#" class="to_cart" data-id="<?php echo $product['id'];?>">до кошику</a>
            </div>
        </div>
    </div>
</div>
<div class="full_description"><?php echo $product['description'];?></div>

<?php include ROOT . '/views/layouts/footer.php'; ?>