<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="big_text center">Вітаю, адмін!</div>
<div class="small_text center">Ваші можливості:</div>
<div class="messages center">
<ul>
    <li><a class="admin_menu no_link" href="/admin/product">Управління товарами</a></li>
    <li><a class="admin_menu no_link" href="/admin/category">Управління категоріями</a></li>
    <li><a class="admin_menu no_link" href="/admin/order">Управління замовленнями</a></li>
    <li><a class="admin_menu no_link" href="/admin/novelty">Управління новинами</a></li>
</ul>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>

