<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="center"><a href="/admin/product/create" class="small_text">Додати товар</a></div>
            
<div class="big_text center">Список товарів:</div>

<br/>

<table class="center">
    <tr>
        <th>id товару</th>
        <th>Назва товару</th>
        <th>Ціна</th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($productsList as $product): ?>
        <tr>
            <td><?php echo $product['id']; ?></td>
            <td><a class="no_link" href="/product/<?php echo $product['id'] ;?>"><?php echo $product['name']; ?></a></td>
            <td><?php echo $product['cost']; ?></td>
            <td><a class="no_link" href="/admin/product/update/<?php echo $product['id']; ?>" title="змінити">змінити</i></a></td>
            <td><a class="no_link" href="/admin/product/delete/<?php echo $product['id']; ?>" title="видалити">видалити</i></a></td>
        </tr>
    <?php endforeach; ?>
</table>


<?php include ROOT . '/views/layouts/footer.php'; ?>

