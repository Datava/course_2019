<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="center"><a class="small_text" href="/admin/">Адмін-панель</a></div>
<br/>

<div class="big_text center">Додати новий товар</div>

<br/>

<?php if (isset($errors) && is_array($errors)): ?>
    <ul class="messages center">
        <?php foreach ($errors as $error): ?>
            <li> - <?php echo $error; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<form class="reg_form center" action="#" method="post" enctype="multipart/form-data">

    <div class="center">Назва товару</div>
    <input type="text" name="name" placeholder="" value="">

    <div class="center">Вартість</div>
    <input type="number" name="cost" placeholder="" value="">

    <div class="center">Категорія</div>
    <select class="small_text center" name="category_id">
        <?php if (is_array($categoriesList)): ?>
            <?php foreach ($categoriesList as $category): ?>
                <option value="<?php echo $category['id']; ?>">
                    <?php echo $category['name']; ?>
                </option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>

    <div class="center">Виробник</div>
    <input type="text" name="brand" placeholder="" value="">

    <div class="center">Розмір</div>
    <input type="text" name="size" placeholder="" value="">

    <div class="center">Країна</div>
    <input type="text" name="country" placeholder="" value="">

    <div class="center">Матеріали</div>
    <input type="text" name="materials" placeholder="" value="">

    <div class="center">Зображення</div>
    <input type="text" name="image" placeholder="" value="">

    <div class="center">Опис</div>
    <textarea name="description"></textarea>

    <div class="center">Популярність</div>
    <input type="number" name="popularity" placeholder="" value="">

    <input type="submit" name="submit" id="reg_btn" value="Зберегти">

</form>


<?php include ROOT . '/views/layouts/footer.php'; ?>

