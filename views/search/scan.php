<?php include ROOT . '/views/layouts/header.php'; ?>

<form class="center reg_form"  method="post">
    <input type="text" name="search" value="<?php echo $search;?>">
    <input type="submit" name="submit" id="reg_btn" value="Знайти" />
</form>

<div class="big_text center">Результати пошуку:</div>
<?php if($searchList): ?>
    <div class="flex_row">
        <?php foreach ($searchList as $product):?>
            <div class="product">
                <a href="/product/<?php echo $product['id'] ;?>"><img alt="<?php echo $product['name'];?>" src="/upload/images/products/<?php echo $product['image'];?>"></a>
                <a class="product_name" href="/product/<?php echo $product['id'] ;?>"><?php echo $product['name'];?></a>
                <div class="product_info">Виробник: <?php echo $product['brand'];?></div>
                <div class="product_info">Країна: <?php echo $product['country'];?></div>
                <div class="cost center"><?php echo $product['cost'];?>-</div>
                <a href="#" class="to_cart center" data-id="<?php echo $product['id'];?>">до кошику</a>
            </div>
        <?php endforeach; ?>
    </div>
<? else: ?>
<div class="small_text center">Товарів не знайдено</div>
<?php endif ?>



<?php include ROOT . '/views/layouts/footer.php'; ?>
