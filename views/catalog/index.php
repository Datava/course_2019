<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="flex_row">
<?php foreach ($categoriesWithOneProduct as $product):?>
    <div class="category">
        <a href="/category/<?php echo $product['category_id']; ?>" class="link big_text center"><?php echo $product['category_name'];?></a>
        <div class="product">
            <?php if (!empty($product['product_id'])): ?>
                <a href="/product/<?php echo $product['product_id'];?>"><img alt="<?php echo $product['product_name'];?>" src="/upload/images/products/<?php echo $product['image'];?>"></a>
                <a class="product_name" href="/product/<?php echo $product['product_id'];?>"><?php echo $product['product_name'];?></a>
                <div class="product_info">Виробник: <?php echo $product['brand'];?></div>
                <div class="product_info">Країна: <?php echo $product['country'];?></div>
                <div class="cost center"><?php echo $product['cost'];?>-</div>
                <a href="#" class="to_cart center" data-id="<?php echo $product['product_id'];?>">до кошику</a>
            <?php else: ?>
                <div class="small_text">Товарів поки немає</div>
            <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>