<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="big_text center"><?php echo $category['name'];?></div>

<div class="flex_row">
    <?php if (!empty($categoryProducts)): ?>
        <?php foreach ($categoryProducts as $product):?>
            <div class="product">
                <a href="/product/<?php echo $product['id'] ;?>"><img alt="<?php echo $product['name'];?>" src="/upload/images/products/<?php echo $product['image'];?>"></a>
                <a class="product_name" href="/product/<?php echo $product['id'] ;?>"><?php echo $product['name'];?></a>
                <div class="product_info">Виробник: <?php echo $product['brand'];?></div>
                <div class="product_info">Країна: <?php echo $product['country'];?></div>
                <div class="cost center"><?php echo $product['cost'];?>-</div>
                <a href="#" class="to_cart center" data-id="<?php echo $product['id'];?>">до кошику</a>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="small_text">Товарів поки немає</div>
    <?php endif; ?>
</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>