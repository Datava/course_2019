<?php
class Novelty
{
	public static function getNewsItemByID($id)
	{
		$id = intval($id);

		if ($id) {
			$db = Db::getConnection();
			$result = $db->query('SELECT * FROM news WHERE id=' . $id);

			$result->setFetchMode(PDO::FETCH_ASSOC);

			$newsItem = $result->fetch();

			return $newsItem;
		}

	}

	public static function getNewsList() {

		$db = Db::getConnection();
		$newsList = array();

		$result = $db->query('SELECT id, text, alt, image, link FROM news');

		$i = 0;
		while($row = $result->fetch()) {
			$newsList[$i]['id'] = $row['id'];
			$newsList[$i]['text'] = $row['text'];
			$newsList[$i]['alt'] = $row['alt'];
			$newsList[$i]['image'] = $row['image'];
			$newsList[$i]['link'] = $row['link'];
			$i++;
		}

		return $newsList;
    }

    public static function createNews($text, $image, $alt)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO news(text, image, alt) '
            . 'VALUES (:text, :image, :alt)';

        $result = $db->prepare($sql);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':image', $image, PDO::PARAM_STR);
        $result->bindParam(':alt', $alt, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function deleteNoveltyById($id)
    {
        $db = Db::getConnection();

        $sql = 'DELETE FROM news WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateNoveltyById($id, $text, $image, $alt)
    {
        $db = Db::getConnection();

        $sql = "UPDATE news
            SET 
                text = :text,
                image = :image,
                alt = :alt
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':image', $image, PDO::PARAM_STR);
        $result->bindParam(':alt', $alt, PDO::PARAM_STR);
        return $result->execute();
    }

}