<?php
class Product
{

    const SHOW_BY_DEFAULT = 3;

    public static function getPopularProducts($count = self::SHOW_BY_DEFAULT)
    {
        $db = Db::getConnection();

        $sql = 'SELECT id, name, cost, brand, country, image FROM product '
                . 'ORDER BY popularity DESC '
                . 'LIMIT :count';

        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['cost'] = $row['cost'];
            $productsList[$i]['image'] = $row['image'];
            $productsList[$i]['brand'] = $row['brand'];
            $productsList[$i]['country'] = $row['country'];
            $i++;
        }
        return $productsList;
    }

    public static function AddPopularity($id) {
        $db = Db::getConnection();

        $sql = "UPDATE product SET popularity = popularity + 1 WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    public static function searchProductsByName($search)
    {
        $db = Db::getConnection();
        $searchList = array();

        $sql = "SELECT id, name, cost, brand, country, image FROM product WHERE name LIKE '%$search%' LIMIT 0,4";

        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        $i = 0;
        while($row = $result->fetch()) {
            $searchList[$i]['id'] = $row['id'];
            $searchList[$i]['name'] = $row['name'];
            $searchList[$i]['cost'] = $row['cost'];
            $searchList[$i]['image'] = $row['image'];
            $searchList[$i]['brand'] = $row['brand'];
            $searchList[$i]['country'] = $row['country'];
            $i++;
        }

        return $searchList;
    }


    public static function getProductsListByCategory($categoryId)
    {

        $db = Db::getConnection();

        $sql = 'SELECT id, name, cost, country, brand, image FROM product '
                . 'WHERE category_id = :category_id '
                . 'ORDER BY popularity DESC';

        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);

        $result->execute();

        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['cost'] = $row['cost'];
            $products[$i]['country'] = $row['country'];
            $products[$i]['brand'] = $row['brand'];
            $products[$i]['image'] = $row['image'];
            $i++;
        }
        return $products;
    }

    public static function getProductById($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM product WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    public static function getProductsList($order = 'popularity')
    {
        $db = Db::getConnection();

        switch ($order) {
            case 'popularity':
                $result = $db->query('SELECT id, name, cost FROM product ORDER BY popularity DESC');
                break;
            case 'id':
                $result = $db->query('SELECT id, name, cost FROM product ORDER BY id ASC');
                break;
        }

        $productsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['cost'] = $row['cost'];
            $i++;
        }
        return $productsList;
    }

    public static function getProdustsByIds($idsArray)
    {
        $db = Db::getConnection();

        $idsString = implode(',', $idsArray);

        $sql = "SELECT * FROM product WHERE id IN ($idsString)";

        $result = $db->query($sql);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['cost'] = $row['cost'];
            $i++;
        }
        return $products;
    }

    public static function deleteProductById($id)
    {
        $db = Db::getConnection();

        $sql = 'DELETE FROM product WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateProductById($id, $options)
    {
        $db = Db::getConnection();

        $sql = "UPDATE product
            SET 
                name = :name, 
                cost = :cost, 
                brand = :brand,
                size = :size,
                materials = :materials,
                country = :country,
                description = :description,
                image = :image,
                popularity = :popularity,
                category_id = :category_id
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':cost', $options['cost'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':size', $options['size'], PDO::PARAM_STR);
        $result->bindParam(':materials', $options['materials'], PDO::PARAM_STR);
        $result->bindParam(':country', $options['country'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':image', $options['image'], PDO::PARAM_STR);
        $result->bindParam(':popularity', $options['popularity'], PDO::PARAM_INT);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createProduct($options)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO product '
                . '(name, cost, brand, size, materials, country, description, image, popularity, category_id)'
                . 'VALUES '
                . '(:name, :cost, :brand, :size, :materials, :country, :description, :image, :popularity, :category_id)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':cost', $options['cost'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':size', $options['size'], PDO::PARAM_STR);
        $result->bindParam(':materials', $options['materials'], PDO::PARAM_STR);
        $result->bindParam(':country', $options['country'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':image', $options['image'], PDO::PARAM_STR);
        $result->bindParam(':popularity', $options['popularity'], PDO::PARAM_INT);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

}
