<?php
class SiteController
{

    public function actionIndex()
    {
        $ses = Cart::countItems();
        $categories = Category::getCategoriesList();

        $popularProducts = Product::getPopularProducts(6);

        $newsList = array();
        $newsList = Novelty::getNewsList();

        $search = false;

        require_once(ROOT . '/views/site/index.php');
        return true;
    }

    public function actionContact()
    {
        require_once(ROOT . '/views/site/contact.php');
        return true;
    }

    public function actionAbout()
    {
        require_once(ROOT . '/views/site/about.php');
        return true;
    }

}
