<?php

class SearchController {

    public function actionScan()
    {
        $searchList = false;
        $search = false;

        if (isset($_POST['submit'])) {
            $search = $_POST['search'];
            $searchList = Product::searchProductsByName($search);
        }

        require_once(ROOT . '/views/search/scan.php');
        return true;
    }

}