<?php
class CatalogController
{

    public function actionIndex()
    {
        $categories = Category::getCategoriesList();

        $categoriesWithOneProduct = array();
        $i = 0;
        foreach ($categories as  $category) {
            $categoryProducts = Product::getProductsListByCategory($category['id']);
            $categoriesWithOneProduct[$i]['category_id'] = $category['id'];
            $categoriesWithOneProduct[$i]['category_name'] = $category['name'];

            if (!empty($categoryProducts[0]['id'])) {
                $categoriesWithOneProduct[$i]['product_id'] = $categoryProducts[0]['id'];
                $categoriesWithOneProduct[$i]['product_name'] = $categoryProducts[0]['name'];
                $categoriesWithOneProduct[$i]['cost'] = $categoryProducts[0]['cost'];
                $categoriesWithOneProduct[$i]['brand'] = $categoryProducts[0]['brand'];
                $categoriesWithOneProduct[$i]['country'] = $categoryProducts[0]['country'];
                $categoriesWithOneProduct[$i]['image'] = $categoryProducts[0]['image'];
            }
            $i++;
        }

        require_once(ROOT . '/views/catalog/index.php');
        return true;
    }

    public function actionCategory($categoryId)
    {
        $category = Category::getCategoryById($categoryId);
        $categoryProducts = Product::getProductsListByCategory($categoryId);

        require_once(ROOT . '/views/catalog/category.php');
        return true;
    }

}
