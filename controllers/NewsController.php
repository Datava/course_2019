<?php

class NewsController {

	public function actionIndex()
	{
		
		$newsList = array();
		$newsList = Novelty::getNewsList();

		require_once(ROOT . '/views/news/index.php');

		return true;
	}

    public function actionView($id)
    {
        if ($id) {
            $newsItem = Novelty::getNewsItemByID($id);

            require_once(ROOT . '/views/news/view.php');

            /*			echo 'actionView'; */
        }

        return true;

    }

}

