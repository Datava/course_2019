<?php
class   UserController
{
    public function actionRegister()
    {
        $name = false;
        $email = false;
        $password = false;
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = "Ім'я має бути довше одного символу!";
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильний email';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль має бути не коротше 6 символів';
            }
            if (User::checkEmailExists($email)) {
                $errors[] = 'Користувач з таким email вже зареєстрований!';
            }
            
            if ($errors == false) {
                $result = User::register($name, $email, $password);
            }
        }

        require_once(ROOT . '/views/user/register.php');
        return true;
    }

    public function actionLogin()
    {
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkEmail($email)) {
                $errors[] = 'Невірний e-mail';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Мінімальна довжина паролю - 6 символів';
            }

            $userId = User::checkUserData($email, $password);

            if ($userId == false) {
                $errors[] = 'Ви невірно ввели дані для входу';
            } else {
                User::auth($userId);

                header("Location: /cabinet");
            }

        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }

    public function actionLogout()
    {
        unset($_SESSION["user"]);

        header("Location: /");
    }

}
