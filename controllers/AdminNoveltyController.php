<?php
class AdminNoveltyController extends AdminBase
{
    public function actionIndex()
    {
        self::checkAdmin();

        $newsList = Novelty::getNewsList();

        require_once(ROOT . '/views/admin_novelty/index.php');
        return true;
    }

    public function actionCreate()
    {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $alt = $_POST['alt'];
            $text = $_POST['text'];
            $image = $_POST['image'];

            $errors = false;

            if ($errors == false) {
                Novelty::createNews($text, $image, $alt);

                header("Location: /admin/novelty");
            }
        }

        require_once(ROOT . '/views/admin_novelty/create.php');
        return true;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            Novelty::deleteNoveltyById($id);

            header("Location: /admin/novelty");
        }

        require_once(ROOT . '/views/admin_novelty/delete.php');
        return true;
    }

    public function actionUpdate($id)
    {
        self::checkAdmin();

        $novelty = Novelty::getNewsItemByID($id);

        if (isset($_POST['submit'])) {
            $alt = $_POST['alt'];
            $text = $_POST['text'];
            $image = $_POST['image'];

            Novelty::updateNoveltyById($id, $text, $image, $alt);

            header("Location: /admin/novelty");
        }

        require_once(ROOT . '/views/admin_novelty/update.php');
        return true;
    }

}
