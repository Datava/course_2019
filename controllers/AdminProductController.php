<?php
class AdminProductController extends AdminBase
{

    public function actionIndex()
    {
        self::checkAdmin();

        $productsList = Product::getProductsList('id');

        require_once(ROOT . '/views/admin_product/index.php');
        return true;
    }

    public function actionCreate()
    {
        self::checkAdmin();

        $categoriesList = Category::getCategoriesListAdmin();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['cost'] = $_POST['cost'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['size'] = $_POST['size'];
            $options['materials'] = $_POST['materials'];
            $options['country'] = $_POST['country'];
            $options['description'] = $_POST['description'];
            $options['image'] = $_POST['image'];
            $options['popularity'] = $_POST['popularity'];
            $options['description'] = $_POST['description'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заполните поля';
            }

            if ($errors == false) {
                $id = Product::createProduct($options);

                header("Location: /admin/product");
            }
        }

        require_once(ROOT . '/views/admin_product/create.php');
        return true;
    }

    public function actionUpdate($id)
    {
        self::checkAdmin();

        $categoriesList = Category::getCategoriesListAdmin();

        $product = Product::getProductById($id);

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['cost'] = $_POST['cost'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['size'] = $_POST['size'];
            $options['materials'] = $_POST['materials'];
            $options['country'] = $_POST['country'];
            $options['description'] = $_POST['description'];
            $options['image'] = $_POST['image'];
            $options['popularity'] = $_POST['popularity'];
            $options['description'] = $_POST['description'];

            Product::updateProductById($id, $options);

            header("Location: /admin/product");
        }

        require_once(ROOT . '/views/admin_product/update.php');
        return true;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            Product::deleteProductById($id);

            header("Location: /admin/product");
        }

        require_once(ROOT . '/views/admin_product/delete.php');
        return true;
    }

}
